from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import app, db
from app.config import Config


import os

app.config.from_object(Config)

migrate = Migrate(app, db)
manager = Manager(app)

@manager.command
def hello():
    print "hello"

if __name__ == "__main__":
    manager.run()
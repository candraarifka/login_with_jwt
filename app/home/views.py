from flask import Blueprint, render_template, request
from app import db, share
from app.user.models import User, Todo

home_blueprint = Blueprint(
    'home', __name__,
    template_folder='templates'
)


@home_blueprint.route('/', methods=['GET'])
def base():
    return 'oke'

import os

from flask import Flask
from flask_share import Share
from flask_sqlalchemy import SQLAlchemy
from app.config import Config

app = Flask(__name__)
app.config.from_object(Config)

db =  SQLAlchemy(app)
share = Share(app)


from app.home.views import home_blueprint 
from app.user.views import user_blueprint 

app.register_blueprint(home_blueprint)
app.register_blueprint(user_blueprint)